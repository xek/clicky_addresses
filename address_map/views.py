from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.shortcuts import render, redirect

from clicky_addresses import settings
from .models import FusionTable, Address
from .forms import AddressForm
from .google import insert_row, clear_table


def index(request):
    """ Main view - displays one page app with added addresses and map """
    ctx = {
        'address_list': Address.objects.order_by('-id'),
        'fusion_table': FusionTable.objects.last().table_id,
        'api_key': settings.GOOGLE_API_KEY
    }
    return render(request, 'index.html', ctx)


def click(request):
    """ Adding new address after user clicks on the map """
    if request.is_ajax() and request.method == 'POST':
        form = AddressForm(request.POST)
        if form.is_valid():
            address = form.save(commit=False)
            try:  # do full_clean, validating fields not present in the form
                address.full_clean()
            except ValidationError as e:
                return JsonResponse({'errors': '; '.join(e.messages)})
            insert_row(address.table.table_id,
                       '{} {}'.format(address.lat, address.lng),
                       address.address, address.date)
            address.save()
            return JsonResponse({'place_id': address.place_id,
                                 'address': address.address})
        return JsonResponse({'errors': form.errors.as_text()})
    return JsonResponse({'errors': 'method not allowed'}, status=405)


def clear(request):
    """ Clear the Google Fusion table and local data, refresh """
    table = FusionTable.objects.last()
    clear_table(table.table_id)
    table.address_set.all().delete()
    return redirect('/')
