from unittest import mock

from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, RequestFactory

from clicky_addresses import settings
from .views import index, click, clear


class BasicTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_index_with_api_key(self):
        request = self.factory.get('/')
        request.user = AnonymousUser()
        response = index(request)
        self.assertEqual(response.status_code, 200)
        result = str(response.content)
        self.assertTrue(settings.GOOGLE_API_KEY in result)

    @mock.patch('address_map.google.insert_row')
    @mock.patch('address_map.google.reverse_geocode')
    def test_click(self, reverse_geocode, insert_row):
        request = self.factory.get('/')
        request.user = AnonymousUser()
        response = click(request)
        self.assertEqual(response.status_code, 405)
        result = str(response.content)
        self.assertTrue('"errors"' in result)

    @mock.patch('address_map.google.clear_table')
    def test_clear(self, clear_table):
        request = self.factory.get('/')
        request.user = AnonymousUser()
        response = clear(request)
        self.assertEqual(response.status_code, 302)
