from django.apps import AppConfig
from django.db.models.signals import post_migrate

from clicky_addresses import settings
from .google import create_table


def create_fusion_tables(sender, **kwargs):
    from .models import FusionTable
    if not settings.TESTING:
        FusionTable('testing').save()
    if FusionTable.objects.count():
        return
    table_id = create_table('clicky_addresses', 'Clicky Addresses', [
        {'name': 'location', 'type': 'LOCATION'},
        {'name': 'address', 'type': 'STRING'},
        {'name': 'date', 'type': 'DATETIME'}])
    FusionTable(table_id=table_id).save()


class AddressMapConfig(AppConfig):
    name = 'address_map'

    def ready(self):
        post_migrate.connect(create_fusion_tables, sender=self)
