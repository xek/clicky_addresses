from googleapiclient.discovery import build
import googlemaps
from oauth2client.service_account import ServiceAccountCredentials

from clicky_addresses import settings


credentials = ServiceAccountCredentials.from_json_keyfile_name(
    settings.GOOGLE_ACCOUNT_KEY, scopes=[
        'https://www.googleapis.com/auth/fusiontables',
        'https://www.googleapis.com/auth/drive'])
fusiontables = build(serviceName='fusiontables', version='v2',
                     credentials=credentials)
drive = build(serviceName='drive', version='v3', credentials=credentials)


def create_table(name, description, columns):
    """
    This creates a Google Fusion Table according to parameters, see:
    https://developers.google.com/fusiontables/docs/v2/using#CreatingTables
    https://developers.google.com/fusiontables/docs/v2/using#WorkingColumns
    Returns a Fusion Table ID.
    """
    # create table
    table = fusiontables.table()
    result = table.insert(body={
        'name': name, 'description': description,
        'columns': columns, 'isExportable': True}).execute()

    # set drive permissions
    permissions = drive.permissions()
    permissions.create(
        fileId=result["tableId"],
        body={"emailAddress": settings.GMAIL,
              "type": "user", "role": "writer"},
        sendNotificationEmail=False).execute()
    permissions.create(
        fileId=result["tableId"],
        body={"value": "me", "withLink": True,
              "type": "anyone", "role": "reader"},
        sendNotificationEmail=False).execute()

    return result["tableId"]


def insert_row(table, location, address, date):
    """
    This inserts data to a Google Fusion Table according to it's ID, see:
    https://developers.google.com/fusiontables/docs/v2/using#insertRow
    """
    query = fusiontables.query()
    return query.sql(sql="INSERT INTO {} (location, address, date)"
                     " VALUES ('{}', '{}', '{}');".format(
                         table, location, address, date)).execute()


def clear_table(table):
    """
    This inserts data to a Google Fusion Table according to it's ID, see:
    https://developers.google.com/fusiontables/docs/v2/using#deleteRow
    """
    query = fusiontables.query()
    return query.sql(sql="DELETE FROM {};".format(table)).execute()


def reverse_geocode(lat, lon):
    """
    Provided a latitude and longitude, return a Google Maps reverse
    geocoding dict. Return only precise building addresses.
    """
    gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)

    # Look up an address with reverse geocoding
    return gmaps.reverse_geocode(
        (lat, lon),
        result_type='street_address|natural_feature',
        location_type='ROOFTOP')
