from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from .google import reverse_geocode


class FusionTable(models.Model):
    table_id = models.CharField(max_length=256)

    def __str__(self):
        return self.table_id


class Address(models.Model):
    place_id = models.CharField(max_length=256, unique=True)
    lat = models.DecimalField(max_digits=20, decimal_places=16)
    lng = models.DecimalField(max_digits=20, decimal_places=16)
    address = models.TextField()
    table = models.ForeignKey(FusionTable, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)

    def clean(self):
        """ Validate the coordinates and fill the address column. """
        if not self.lat or not self.lng:
            raise ValidationError(_("Coordinates are required"))
        address = reverse_geocode(self.lat, self.lng)
        # we are only concerned with street addresses, so filter out others
        address = [a for a in address if a['types'] == ['street_address']]
        if len(address) < 1:
            raise ValidationError(_("No address found at this location"))
        address = address[0]
        self.address = address['formatted_address']
        self.place_id = address['place_id']
        self.table = FusionTable.objects.last()

    def __str__(self):
        return self.address
