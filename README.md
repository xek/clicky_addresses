Clicky Addresses Django App
===========================

Task
----

Build a single page app with a full viewport map on top and a list of
addresses below based on Django 1.9.

Note: Django 1.9 is insecure and is no longer supported.
Please upgrade to a newer release!


Use case
--------

If I click any location on the map: validate that this has a real address and
it’s not some wood/mountain/ocean, etc. 

If valid, save it to a simple db table with lat, lon, address (can be single
string) and also to google fusion tables (decide what data). 

Have a marker appear instantly after the click on the map based on the google
fusion table data. 

Update the list of addresses underneath the map with the location where you
clicked. 

Duplicates on google fusion table are not allowed. 

I can reset all data on google fusion tables and the database and start fresh.

Please note: solution can be ugly, but code quality, architecture,
completeness should be as good as you can.
This solution is doable in 3-4 hours.


Setup & installation
====================

In order to access to Google Geolocation, Fusion Tables and Google Drive APIs,
add them in your Google APIs Console and create credentials.
You need a `GOOGLE_API_KEY` (for Geolocation) and a `GOOGLE_ACCOUNT_KEY`
(for accessing Fusion Tables from your web server).

Set those, together with your `GMAIL` address at the bottom of settings.py

Run unit tests and pep8 syntax checks with tox.


Criteria for the evaluation
---------------------------

- documentation
- do others understand the code without knowing the task
- architecture / performance / code structure
- bugs?
- task completely done?
- communication before and after the task
- runs out of the box with `virtualenv -p python3 .ve && pip install -r requirements.txt && ./manage.py [syncdb|migrate|runserver]` on python3/sqlite


My comments
-----------

I think future version could just extract buildings from OSM:
https://github.com/tudelft3d/3dfier/wiki/Extracting-building-footprints-from-OpenStreetMap
It should also show OSM maps to be consistent. This is because google returns building addresses in strange locations, especially in dense cities.

Google Fusion Tables are laggy and not live, so there is no way to have a
marker appear instantly after the click on the map based on the google
fusion table data. I use the response value from Django server to draw the
marker instead.

The application is laggy because external services are used, this can be
fixed by using OSM data and storing the data locally.

Data between the internal and external tables should be periodically synced.
